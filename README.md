# GWGXG - An attempt to create a gemini client for [twtxt](https://github.com/buckket/twtxt)

Currently a work in progress, and will not work. at all

## Config File
gwgxg looks for a config file in
`$XDG_CONFIG_HOME/gwgxg.conf.json`. It will fallback to
`$HOME/.config/gwgxg.conf.json` if the aforementioned is not found

It reads an array of urls from the `urls` section

An example is available in [/gwgxg.conf.json]

The program requires this file to be present for it to work

