use crate::ui::UiError::*;
use std::io;

/// A struct defining a single option that can be chosen in a prompt
///
/// repchar is the actual character to be entered, and what is displayed in the main prompt
/// info is only displayed if the user enters `?`
#[derive(Debug, Clone, PartialEq)]
pub struct PromptOption {
    pub repchar: char, // The char that represents it
    pub info: String,
}

impl PromptOption {
    pub fn new(repchar: char, info: String) -> PromptOption {
        PromptOption { repchar, info }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn sanity_check() {
        use super::*;
        let a = PromptOption {
            repchar: 'a',
            info: "INFO".to_string(),
        };

        let b = PromptOption::new('a', "INFO".to_string());

        assert_eq!(a, b);
    }
}

#[derive(Debug)]
pub enum UiError {
    ManyItemsFoundError,
    FlushError,
    ReadLineError,
    IntParseError,
}

/// Create a new prompt for the user to enter information to
///
/// Displays the prompt as well as all the repchars for each option they can select
/// Allows the user to select `?` to get a fill list of things they may choose to enter
pub fn prompt(p: String, options: Vec<PromptOption>) -> Result<PromptOption, UiError> {
    let mut opts = String::from("[");
    for opt in &options {
        opts = opts + format!("{},", &opt.repchar).as_str();
    }
    opts += "?]";

    loop {
        let input = match get_input(format!("{} {} ", p, opts)) {
            Ok(t) => t,
            Err(e) => return Err(e),
        };

        if input.len() > 1 {
            println!("Please enter a single character");
            continue;
        }
        if input.len() == 0 {
            println!("Please enter a selection");
            continue;
        }

        if input == "?" {
            println!("--- Help ---");
            for opt in &options {
                println!("{}: {}", opt.repchar, opt.info);
            }
            continue;
        }

        let mut iter: Vec<PromptOption> = options
            .clone() // We clone because otherwise it won't still be there if theres no match, and then `?` wont work
            .drain(..)
            .filter(|opt| opt.repchar == input.chars().next().unwrap())
            .collect();

        // If there was no match
        if iter.len() == 0 {
            println!("This was not a valid option");
            continue;
        };

        if iter.len() > 1 {
            return Err(ManyItemsFoundError);
        }

        return Ok(iter.remove(0));
    }
}

/// Get input from a user by diplaying a prompt
///
/// Also provides a flush to get round a wierd error that can be gotten if stdout is buffered, making the prompt not get shown
pub fn get_input(prompt: String) -> Result<String, UiError> {
    print!("{prompt}");
    // Fix a weird thing that stdout is buffered for """performace reasons""" and therefore is buggy
    match io::Write::flush(&mut io::stdout()) {
        Ok(_) => (),
        Err(_) => return Err(FlushError),
    }

    let mut input = String::new();

    match io::stdin().read_line(&mut input) {
        Ok(_) => (),
        Err(_) => return Err(ReadLineError),
    }

    Ok(input.trim().to_string())
}

/// Get an integer from stdin
///
/// Uses `get_input`, but provides additional things to enure it is casted as a usize, and catches errors if it is not
pub fn get_input_int(prompt: String) -> Result<usize, UiError> {
    let input = match get_input(prompt) {
        Ok(t) => t,
        Err(e) => return Err(e),
    };

    let int: usize = match input.parse() {
        Ok(t) => t,
        Err(_) => return Err(IntParseError),
    };

    Ok(int)
}
