use rustls::Session;
use rustls::{Certificate, RootCertStore, ServerCertVerified, ServerCertVerifier, TLSError};
use std::io::{Read, Write};
use std::net::TcpStream;
//use std::os::unix::net::AncillaryData;
use crate::req::ReqError::*;
use std::sync::Arc;
use webpki::DNSNameRef;
//use std::result;

#[derive(Debug)]
pub enum ReqError {
    TlsStreamWriteError,
    InvalidResponseError,
    InvalidDNSNameError,
    SocketConnectionError,
    ProcessTlsError,
    ReadTlsError,
}

/// Represents a request that will be made, to some url
pub struct GeminiReq {
    pub url: Url,
}

impl GeminiReq {
    pub fn new(url: Url) -> Self {
        Self { url }
    }

    /// Make the request to `self.url`
    pub fn make_req(&mut self) -> std::result::Result<String, ReqError> {
        let mut cfg = rustls::ClientConfig::new();
        let mut config = rustls::DangerousClientConfig { cfg: &mut cfg };
        let dummy_verifier = Arc::new(DummyVerifier::new());
        config.set_certificate_verifier(dummy_verifier);

        let rc_config = Arc::new(cfg);
        let dns_url = self.url.for_dns();
        let dns_name = match webpki::DNSNameRef::try_from_ascii_str(&dns_url) {
            Ok(t) => t,
            Err(_) => return Err(InvalidDNSNameError),
        };

        let mut client = rustls::ClientSession::new(&rc_config, dns_name);
        let mut socket = match TcpStream::connect(self.url.for_tcp()) {
            Ok(t) => t,
            Err(_) => return Err(SocketConnectionError),
        };

        let mut stream = rustls::Stream::new(&mut client, &mut socket);
        match stream.write(self.url.request().as_bytes()) {
            Ok(t) => t,
            Err(_) => return Err(TlsStreamWriteError),
        };

        while client.wants_read() {
            match client.read_tls(&mut socket) {
                Ok(t) => t,
                Err(_) => return Err(ReadTlsError),
            };
            match client.process_new_packets() {
                Ok(t) => t,
                Err(_) => return Err(ProcessTlsError),
            };
        }
        let mut data = Vec::new();
        let _ = client.read_to_end(&mut data);

        let mut response = Response::new(String::from_utf8_lossy(&data).to_string());

        //println!("{:#?}", response);

        match response.status.code.chars().next().unwrap() {
            '2' /* OK */ => {
                if response.body == None {
                    match client.read_tls(&mut socket) {
                        Ok(t) => t,
                        Err(_) => return Err(ReadTlsError),
                    };
                    match client.process_new_packets() {
                        Ok(_) => (),
                        Err(_) => return Err(ProcessTlsError),
                    };
                    let mut data = Vec::new();
                    let _ = client.read_to_end(&mut data);

                    response.body = Some(String::from_utf8_lossy(&data).to_string());
                }

                // println!("{}", response.body.unwrap_or("".to_string()))
                // Return the final content of the page that was requested
                Ok(response.body.unwrap_or("".to_string()))

            }
            '3' /* Redirect */ => {
                self.url = Url::new(response.status.meta.as_str());
                println!("INFO: Followed Redirect");
                self.make_req()
            }
            _ => {
                println!(
                    "Error - {} - {}",
                    response.status.code, response.status.meta
                );

                Err(InvalidResponseError)
            },
        }
    }
}

struct DummyVerifier {}

impl DummyVerifier {
    fn new() -> Self {
        DummyVerifier {}
    }
}

impl ServerCertVerifier for DummyVerifier {
    fn verify_server_cert(
        &self,
        _: &RootCertStore,
        _: &[Certificate],
        _: DNSNameRef,
        _: &[u8],
    ) -> Result<ServerCertVerified, TLSError> {
        return Ok(ServerCertVerified::assertion());
    }
}

/// Represents a single URL deconstructed into useful fields
#[derive(Debug, PartialEq)]
pub struct Url {
    scheme: String,
    address: String,
    port: String,
    path: String,
    query: String,
    fragment: String,
}

impl Url {
    /// Parses all the parts of the url
    ///
    /// Defaults to gemini things (eg gemini:// or port 1965) if nothing else is specified
    /// No need to use this for anything but gemini given...everything but the implementation is there if needed, i guess
    pub fn new(url: &str) -> Self {
        let mut url_string = String::from(url);

        let scheme: String;
        let scheme_end_index = url_string.find("://").unwrap_or(0);

        if scheme_end_index == 0 {
            scheme = String::from("gemini");
        } else {
            scheme = url_string.drain(..scheme_end_index).collect();
        }
        url_string = url_string.replacen("://", "", 1);

        let mut address_end_index = url_string.find(":").unwrap_or(0);
        let address: String;
        let port: String;

        if address_end_index != 0 {
            url_string = url_string.replacen(":", "", 1);
            address = url_string.drain(..address_end_index).collect();

            let port_end_index = url_string.find("/").unwrap_or(url_string.len());
            port = url_string.drain(..port_end_index).collect();
            url_string = url_string.replacen("/", "", 1);
        } else {
            address_end_index = url_string.find("/").unwrap_or(url_string.len());
            address = url_string.drain(..address_end_index).collect();
            url_string = url_string.replacen("/", "", 1);

            match scheme.as_str() {
                "gemini" => port = "1965".to_string(),
                "http" => port = "80".to_string(),
                "https" => port = "443".to_string(),
                _ => port = String::new(),
            }
        }

        let path_end_index = url_string.find("?").unwrap_or(url_string.len());
        let path: String = url_string.drain(..path_end_index).collect();
        url_string = url_string.replacen("?", "", 1);

        let query_end_index = url_string.find("#").unwrap_or(url_string.len());
        let query: String = url_string.drain(..query_end_index).collect();
        url_string = url_string.replacen("#", "", 1);

        let fragment = url_string;

        Url {
            scheme,
            address,
            port,
            path,
            query,
            fragment,
        }
    }

    /// Format to the xyz.invalid:1965 scheme
    pub fn for_tcp(&self) -> String {
        format!("{address}:{port}", address = self.address, port = self.port)
    }

    /// Format to xyz.invalid
    pub fn for_dns(&self) -> String {
        format!("{address}", address = self.address)
    }

    /// Format to a request (gemini://xyz.invalid:1965/index.gmi\r\n)
    pub fn request(&self) -> String {
        format!(
            "{scheme}://{address}:{port}/{path}\r\n",
            scheme = self.scheme,
            address = self.address,
            port = self.port,
            path = self.path
        )
    }
}

/// The status returned by a gemini request
#[derive(Debug)]
struct Status {
    code: String,
    meta: String,
}

impl Status {
    fn new(status: String) -> Self {
        let tokens: Vec<&str> = status.splitn(2, " ").collect();
        //println!("{:#?}", tokens);
        Status {
            code: tokens[0].to_string(),
            meta: tokens[1].to_string(),
        }
    }
}

/// Represents the respose from a gemini request
#[derive(Debug)]
#[allow(dead_code)]
struct Response {
    status: Status,
    mime_type: Option<String>,
    charset: Option<String>,
    lang: Option<String>,
    body: Option<String>,
}

impl Response {
    pub fn new(data: String) -> Self {
        let tokens: Vec<&str> = data.splitn(2, "\r\n").collect();
        let status = Status::new(tokens[0].to_string());

        match status.code.chars().next().unwrap() {
            '2' => {
                let mime_type = Some("text/gemini".to_string());
                let charset = Some("utf-8".to_string());
                let lang = Some("en".to_string());

                let body;
                if tokens[1] != "" {
                    body = Some(tokens[1].to_string());
                } else {
                    body = None;
                }

                Response {
                    status,
                    mime_type,
                    charset,
                    lang,
                    body,
                }
            }
            _ => Response {
                status,
                mime_type: None,
                charset: None,
                lang: None,
                body: None,
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn make_req_to_valid_url() {
        let url = Url::new("gemini://tilde.pink/~eden/");
        let mut req = GeminiReq::new(url);
        req.make_req().unwrap();
    }

    #[test]
    fn make_req_and_validate_return() {
        let url = Url::new("gemini://tilde.pink/~eden/gwgxgtests/");
        let mut req = GeminiReq::new(url);
        let ret = req.make_req().unwrap();
        assert_eq!("yo!\n", ret);
    }

    #[test]
    #[should_panic]
    fn make_req_to_invalid_url() {
        let url = Url::new("gemini://xyz.invalid");
        let mut req = GeminiReq::new(url);
        req.make_req().unwrap();
    }

    #[test]
    fn make_req_to_url_other_than_index() {
        let url = Url::new("gemini://tilde.pink/~eden/gwgxgtests/exists.gmi");
        let mut req = GeminiReq::new(url);
        req.make_req().unwrap();
    }

    #[test]
    #[should_panic]
    fn make_req_to_nonexistant_file() {
        let url = Url::new("gemini://tilde.pink/~eden/gwgxgtests/noexist.gmi");
        let mut req = GeminiReq::new(url);
        req.make_req().unwrap();
    }

    #[test]
    fn baseline_all_fields() {
        let url = Url::new("gemini://tilde.pink:1965/~eden/?xyz=whatever#downabit");

        let manual = Url {
            scheme: "gemini".to_string(),
            address: "tilde.pink".to_string(),
            port: "1965".to_string(),
            path: "~eden/".to_string(),
            query: "xyz=whatever".to_string(),
            fragment: "downabit".to_string(),
        };
        assert_eq!(url, manual);
    }

    #[test]
    fn bare_minimum_url() {
        let url = Url::new("tilde.pink");
        let manual = Url {
            scheme: "gemini".to_string(),
            address: "tilde.pink".to_string(),
            port: "1965".to_string(),
            path: String::new(),
            query: String::new(),
            fragment: String::new(),
        };

        assert_eq!(url, manual);
    }

    #[test]
    fn test_tcp_format() {
        let url = Url::new("tilde.pink/~eden");
        assert_eq!(url.for_tcp(), "tilde.pink:1965".to_string());
    }

    #[test]
    fn test_dns_format() {
        let url = Url::new("tilde.pink/~eden");
        assert_eq!(url.for_dns(), "tilde.pink".to_string());
    }

    #[test]
    fn test_request_format() {
        let url = Url::new("tilde.pink/~eden");
        assert_eq!(url.request(), "gemini://tilde.pink:1965/~eden\r\n");
    }
}
