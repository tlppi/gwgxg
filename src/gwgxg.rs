use crate::colours::Colours;
use crate::config::Config;
use crate::gwgxg::GwgxgError::*;
use crate::req;

use chrono::{DateTime, NaiveDateTime};

#[derive(Debug)]
pub enum GwgxgError {
    ReqError,
    ParseDateError,
    NoTabError,
}

/// A single site
///
/// Contains the url of the site, and a vec of `Post`s that it has
#[derive(Clone, Debug)]
pub struct Site<'a> {
    pub url: &'a String,
    posts: Vec<Post>,
}

/// A single post
///
/// Contains the date it was gotten, and the content of the post
/// Date is a Chrono::NaiveDateTime, and so should be parsed to this. Use `Chrono::DateTime::parse_from_rfc3339` to get this from the date in the twtxt format
#[derive(Clone, Debug)]
struct Post {
    date: NaiveDateTime,
    content: String,
}

/// Get all feeds from the urls in the config
pub fn get_feeds(conf: &Config) -> Result<Vec<Site>, GwgxgError> {
    let mut sites: Vec<Site> = Vec::new();

    for url in &conf.urls {
        let mut req = req::GeminiReq::new(req::Url::new(&url));

        let res = match req.make_req() {
            Ok(t) => t,
            Err(_) => return Err(ReqError),
        };

        let mut posts: Vec<Post> = Vec::new();

        for line in res.lines() {
            // We use split_once here so there can be more tabs in the content
            let s: Vec<&str> = match line.split_once('\t') {
                Some((d, c)) => vec![d, c],
                None => return Err(NoTabError),
            };

            let date = match DateTime::parse_from_rfc3339(s[0].trim()) {
                Ok(t) => t.naive_utc(),
                Err(_) => return Err(ParseDateError),
            };

            let post = Post {
                date,
                content: s[1].to_string(),
            };

            posts.push(post);
        }

        let site = Site { url, posts };

        sites.push(site);
    }

    Ok(sites)
}

/// Display all the feeds in a vec passed to it using `crate::Colours` and some tabs to make it look "not too bad" (in my opinion, of course)
pub fn display_feeds(sites: &Vec<Site>) {
    for site in sites {
        println!("{}{}{}", Colours::Blue, site.url, Colours::Reset);
        for post in &site.posts {
            println!(
                "\t{}{}{} {}",
                Colours::Magenta,
                post.date.date(),
                Colours::Reset,
                post.content
            );
        }
    }
}

/// Display a single feed, using `crate::Colours` and some tabs to make it look "kinda okay ig"
pub fn display_feed(site: Site) {
    for post in &site.posts {
        println!(
            "\t{}{}{} {}",
            Colours::Magenta,
            post.date.date(),
            Colours::Reset,
            post.content
        )
    }
}
