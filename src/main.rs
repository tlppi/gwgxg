#![warn(missing_docs)]

use std::env;
use std::process::exit;

mod colours;
mod config;
mod gwgxg;
mod req;
mod ui;

use ui::*;

fn main() {
    let config_path = format!(
        "{}/gwgxg.conf.json",
        match env::var("XDG_CONFIG_HOME") {
            Ok(t) => t,
            Err(_) => {
                println!("INFO: Could not get XDG_CONFIG_HOME. Trying $HOME");
                match env::var("HOME") {
                    Ok(t) => format!("{}/.config", env::var("HOME").unwrap()),
                    Err(_) => {
                        println!("ERROR: Could not read $HOME. Exiting");
                        exit(1);
                    }
                }
            }
        }
    );

    let mut config = match config::read_config(&config_path) {
        Ok(t) => t,
        Err(e) => {
            println!("ERROR: {:#?}", e);
            exit(1);
        }
    };

    'mainuiloop: loop {
        let main_ui_prompts = vec![
            PromptOption::new('n', "Add a new feed to your list".to_string()),
            PromptOption::new('a', "View all feeds on your list".to_string()),
            PromptOption::new('v', "View a specific feed from your list".to_string()),
            PromptOption::new('q', "Quit the app".to_string()),
        ];

        let choice = match prompt(
            "Enter a selection or type `?` to get more info".to_string(),
            main_ui_prompts,
        ) {
            Ok(t) => t,
            Err(e) => {
                println!("ERROR: {:#?}", e);
                exit(1);
            }
        };

        // TODO: Maybe implement some kind of other ID field to match against?
        match choice.repchar {
            'a' => {
                let feeds = match gwgxg::get_feeds(&config) {
                    Ok(t) => t,
                    Err(e) => {
                        println!("ERROR: {:#?}", e);
                        exit(1);
                    }
                };
                gwgxg::display_feeds(&feeds);
            }
            'n' => {
                config.add_url(
                    match get_input(String::from("Enter the URL of the feed: ")) {
                        Ok(t) => t,
                        Err(e) => {
                            println!("ERROR: {:#?}", e);
                            exit(1);
                        }
                    },
                );

                config.write_to_file(&config_path);
            }
            'v' => {
                let feeds = match gwgxg::get_feeds(&config) {
                    Ok(t) => t,
                    Err(e) => {
                        println!("ERROR: {:#?}", e);
                        exit(1);
                    }
                };

                for (i, feed) in feeds.iter().enumerate() {
                    println!("{i}: {}", feed.url);
                }
                let input = match get_input_int("Enter the value of your selection: ".to_string()) {
                    Ok(t) => t,
                    Err(e) => {
                        println!("ERROR: {:#?}", e);
                        exit(1);
                    }
                };

                if input > feeds.len() - 1 {
                    println!("There is no site associated with that number");
                    continue;
                }

                gwgxg::display_feed(feeds[input].clone());
            }
            'q' => break 'mainuiloop,
            _ => {
                println!("Option not recognised");
                exit(1)
            }
        }
    }
}
