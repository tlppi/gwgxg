use std::fmt;

/// Enum allowing us to display colours
///
/// When implementing Display, we use the position of the enum variant to choose what in the `\x1b` range of terminal formatting we wish to use
///
/// Includes `Reset` to go back to the default - always use this when using colours to prevent clogging other output
#[derive(Copy, Clone, Debug)]
#[allow(dead_code)]
pub enum Colours {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    BrightBlack,
    BrightRed,
    BrightGreen,
    BrightYellow,
    BrightBlue,
    BrightMagenta,
    BrightCyan,
    Reset,
}

impl fmt::Display for Colours {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let code = match *self as u8 {
            0..=7 => format!("\x1b[3{}m", *self as u8),
            8..=14 => format!("\x1b[9{}m", *self as u8),
            15 => "\x1b[0m".to_string(),
            _ => panic!("forgot to add a new case for match"), // We should never reach this
        };
        write!(f, "{}", code)
    }
}
