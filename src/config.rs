use serde::{Deserialize, Serialize};
use std::fs;
use std::io::prelude::*;

use crate::config::ConfigError::*;

#[derive(Debug)]
pub enum ConfigError {
    FileOpenError,
    JsonParseError,
    JsonSerializeError,
    FileWriteError,
}

/// A struct representing the config
///
/// Should probably be deserialised in from json using the `read_config` method
#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub urls: Vec<String>,
}

impl Config {
    /// Helper to add a url to the configs own list
    ///
    /// Make sure to serialise it back to json using write_to_file()
    pub fn add_url(&mut self, s: String) {
        self.urls.push(s);
    }

    /// Write the current config back to a file
    ///
    /// Overwrites anything currently in said file
    pub fn write_to_file(&self, path: &String) -> Option<ConfigError> {
        let mut file = match fs::File::create(path) {
            Ok(t) => t,
            Err(_) => return Some(FileOpenError),
        };

        match file.write_all(
            match serde_json::to_string_pretty(&self) {
                Ok(t) => t,
                Err(_) => return Some(JsonSerializeError),
            }
            .as_bytes(),
        ) {
            Ok(t) => t,
            Err(e) => {
                println!("{}", e);
                return Some(FileWriteError);
            }
        }

        None
    }
}

/// Read json from a file and match it to a Config
///
/// TODO: Move this into a method of `Config`, where it would probably make more sense
pub fn read_config(config_path: &String) -> Result<Config, ConfigError> {
    let config_str = match fs::read_to_string(config_path) {
        Ok(t) => t,
        Err(_) => return Err(FileOpenError),
    };

    let config: Config = match serde_json::from_str(&config_str) {
        Ok(t) => t,
        Err(_) => return Err(JsonParseError),
    };

    Ok(config)
}
