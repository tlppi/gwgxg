# Developing

So you wanna help? WHY??

I guess if you *really* want to, you could try the following things:
    - Check the PRs - there should be a pending one for the next release
    - There will be a branch associated with a release - `x.y.z-dev`. You are hoping to merge into this, which will be bundled up together and merged together into master
    - Open a draft PR! I always appreciate knowing what people are working on, to see how people want the project to develop. Obviously this isn't *necessary*, but itll help. ALSO, it might make me block the merge until you're done, so you won't have to start targeting against a different release :)
    - When you're done, undraft the PR. Feel free to request a review from me, to bring it to my attn. It makes me feel special :)
    - Feel satisfied that youve helped. EVEN IF i dont merge it (which, lets be honest, i probably will), seeing someone help on a project of mine feels amazing (orr... must feel amazing. Noones really made me feel that special, yet)
